# Taicang Life



## Getting started


Taicang Life 是一个专门为西浦太仓学生搭建的信息交流平台。在这个平台上，你可以发布包括但不限于闲置物品信息、拼车信息、拼好物信息以及寻求帮助的信息。Taicang Life 致力于提供简单而便捷的信息交流服务。你可以通过标签和搜索功能快速检索到相应的信息。我们鼓励大家发布各种信息，但是**我们坚决反对包括但不限于：**
 - 实施任何网络暴力
 - 传播任何不实信息
 - 发布任何煽动性言论
 - 发布任何违反法律法规的信息

Taicang Life 是一个开源的仓库，人人都可以参与贡献，**用户需对自己在Taicang Life 中的一切言行负责**。

**还不会使用Taicang Life？让我们带你快速上手**。

请确保你已经注册了一个极狐GitLab账号。如果还没有，请点击此处[注册账号](https://jihulab.com/users/sign_up)进行注册。

## 使用议题发布信息

Taicang Life 依赖于极狐GitLab提供的DevOps平台。我们使用议题来发布信息。要发布信息，请：

1. 点击界面左侧的<a href="https://jihulab.com/taicang/taicang-life/-/issues" target=_blank>议题（Issue）</a>。

| 电脑端 | 移动端（在页面左上角） |
| ------ | ------ |
| ![图片.png](./static/图片.png) | ![图片.png](./static/议题.png) |

2. 点击右上角<a href="https://jihulab.com/taicang/taicang-life/-/issues/new" target=_blank>新建议题</a>。

| 电脑端 | 移动端 |
| ------ | ------ |
| ![图片-1.png](./static/图片-1.png) | ![图片.png](./static/新建议题.png) |

3. 输入标题，并选择一个模板。

| 电脑端&移动端 |
| ------ |
| ![图片-2.png](./static/图片-2.png) |


4. 分配一个标记。如果你是通过模板创建的议题，请忽略此项。
5. 点击创建议题。

**恭喜你，现在你已经成功的创建了一个议题，发布了信息**。

## 查找需要的信息

还记得在创建议题的时候我们给议题分配了一个标记吗。通过标记，我们可以非常方便的找到我们所需要的信息。以拼车为例：
1. 在<a href="https://jihulab.com/taicang/taicang-life/-/issues" target=_blank>议题</a>界面找到搜索框。

| 电脑端 | 移动端 |
| ------ | ------ |
| ![图片-3.png](./static/图片-3.png) | ![图片.png](./static/搜索议题.png) |

2. 你可以直接输入想要搜索的内容。
3. 你也可以点击搜索框，在下拉菜单中依次选择“标记”、“=”、“你要查找的标记”。

| 电脑端&移动端 |
| ------ |
| ![图片-4.png](./static/图片-4.png) |

除此之外，我们还可以直接点击每一条议题上的标记进行快速跳转。

| 电脑端&移动端 |
| ------ |
| ![图片-5.png](./static/图片-5.png)|

## 联系信息发布者

如果你需要联系信息发布者，你可以直接在议题详情页面下方的评论区添加评论。评论发布之后信息发布者将收到邮件提醒。

## 修改发布的信息

如果你需要修改发布的信息，请点击议题详情页上面的“小铅笔”。
![图片.png](static/修改.png)

## 常见问题
### 1. 在我发布信息后，机器人提醒我为议题添加标记
如果你是第一次发布议题，或者修改了模板中的标记，那么恭喜你，会遇到我们可爱的Taicang-bot🤖。这个时候，请仔细阅读机器人的提示，按照提示为议题添加一个标记。这样有助于大家更快找到你的信息哦！

### 2. 为什么我没办法为议题添加标记
极狐GitLab暂时不支持访客为议题添加标记。因此我们可爱的Taicang-bot🤖会自动为大家提升权限。这个时候只需要刷新一下就可以添加标记了。如果机器人太忙了漏掉了你的信息，请尝试在该议题下直接添加评论，例如添加”拼车“标记，请评论以下内容：label拼车。然后机器人会自动为你加上对应标签，刷新一下即可。如果还是不行，别着急，请向这个邮箱发送一封邮件，contact-project+taicang-taicang-life-49989-issue-@mg.jihulab.com，并附上你注册账号的邮箱或者用户名，我将手动为你开通权限。

你也可以在议题详情页面直接@zzj2。

## 联系我们
如果有任何问题，请向contact-project+taicang-taicang-life-49989-issue-@mg.jihulab.com发送邮件。
你也可以创建一个议题，使用“建议”模板，我们会仔细考虑你的意见。


